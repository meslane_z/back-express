const bodyParser = require("body-parser");
const express = require("express");
const app = express(); app.use(bodyParser.json());
const route = express.Router();
const Port = 8080;

app.use("/", route);
app.listen(Port, () => {
    console.log("App running on Port", Port);
});

const users = [{
    id: "1",
    name: "Zintle",
    email: "meslanezintle@gmail.com",
    password: "654321"
},
{
    id: "2",
    name: "Obakeng",
    email: "obakeng@ty.com",
    password: "876543"
},
{
    id: "3",
    name: "Zandile",
    email: "zann@gmail.com",
    password: "00889"
}];

route.get("/users", (req, res) => {
    res.status(200).send(users);
});

route.get("/users/:email", (req, res) => {
    const email = req.params.email;
    console.log(email);
    const user = users.filter(item => item.email === email);
    res.status(200).send(user);
});

route.get("/login/:email/:password", (req, res) => {
    const email = req.params.email;
    const password = req.params.password;
    const user = users.filter(item => item.email === email && item.password === password);
    res.status(200).send(user);
});

route.post("/register", (req, res) => {
    const { name, email, password } = req.body;
    const user = {
        name: name,
        email: email,
        password: password
    }
    console.log(user);
    users.push(user)
    res.status(200).send(users)
});

route.delete("/delete/:id", (req, res) => {
    const id = req.params.id;
    const user = users.findIndex(item => 
    item.id == id);
    users.splice(user,id)
 res.status(200).send(users)
    });

    route.put("/update/:id", (req,res) => {
        const id = req.params.id;
        const user = users.forEach(item => {
            if(item.id === id){
                item.name = req.body.name,
                item.email = req.body.email,
                item.password = req.body.password
            }
        })
    });
    res.json(users)
    
